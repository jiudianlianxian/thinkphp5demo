
    //20170617 create by zhouqi
    function ca(con){
        layer.msg(con);
        // layer.open({
        //     title: '提示信息'
        //     ,content: con
        // });
    }
    function ca_t(title, con){
        layer.open({
            title : title
            ,content : con
        })
    }
    //输入框浮点数验证
    function clearNoNum(obj){
        //先把非数字的都替换掉，除了数字和.
        obj.value = obj.value.replace(/[^\d.]/g,"");
        //必须保证第一个为数字而不是.
        obj.value = obj.value.replace(/^\./g,"");
        //保证只有出现一个.而没有多个.
        obj.value = obj.value.replace(/\.{2,}/g,".");
        //保证.只出现一次，而不能出现两次以上
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
        //保证 数字整数位不大于8位
        if(100000000<=parseFloat(obj.value))
            obj.value = "";
    }

    function succ(title,msg,url){
        $.gritter.add({
            title:	title,
            text:	msg,
            time: 200,
            speed:1000,
            sticky: false,
            after_close: function(){
                window.location.href=url;
            }
        });
    }

    function err(msg){
        $.gritter.add({
            title:	"错误!",
            text:	msg,
            time: 1000,
            speed:1000,
            sticky: false,
        });
    }
    function jumpUrl(url) {
        if (url) {
            location.href = url;
        } else {
            history.back(-1);
        }
    }
    function error(msg, timeout, callback) {
        var boxHtml = '<div class="baomsgbox"></div>';
        if ($(".baomsgbox").length == 0) {
            $("body").append(boxHtml);
        }
        $(".baomsgbox").html('<img src="' + BAO_PUBLIC + '/Admin/images/wrong.gif" /><span  style=" color: red;">' + msg + '</span>');
        setTimeout(function () {
            lock = 0;
            $(".baomsgbox").hide();
            eval(callback);
        }, timeout ? timeout : 3000);
    }

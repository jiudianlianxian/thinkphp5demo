$(function(){
  $('.btn-success').click(function(event){
        event.preventDefault();  //阻止表单提交
        var account  =  $('#account').val();
        var password =  $('#password').val();
        var code     =  $('#code').val();
        if($.trim(account) == ''){
          layer.alert('账号不可为空！');
          return false;
        }
        if($.trim(password) == ''){
          layer.alert('密码不可为空！');
          return false;
        }
        if($.trim(code) == ''){
          layer.alert('请输入验证码！');
          return false;
        }
        var postUrl = $('#submitForm').attr('action');
        var param = $('#submitForm').serialize();
        var btn = $("input[type='botton']");
        btn.attr('disabled', 'disabled');
        $.ajax({
              url: postUrl,
              type: 'post',
              dataType: 'json',
              data: param
        })
        .done(function(data) {//成功提交后的回调
              layer.alert(data.msg);
              if (data.code == 1) {
                    window.location.href = data.url;
              } else {
                    return false;
              }
        })
        .fail(function() {//提交失败的回调方法
            console.log("error");
            layer.msg('发生错误，请重试');
            return false;
        })
        .always(function() {//提交成功不成功都会执行此函数
              btn.removeAttr('disabled');
        });
  });
  $(document).keydown(function (event) {
      var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
      if (keyCode == 13) {
            $(".btn-success").click();
        }
  });
});







/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : thinkphpdemo

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-03-05 09:27:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for jdlx_recharge
-- ----------------------------
DROP TABLE IF EXISTS `jdlx_recharge`;
CREATE TABLE `jdlx_recharge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `money` decimal(10,2) DEFAULT NULL,
  `status` char(1) DEFAULT NULL COMMENT '状态 1：充值中 2：充值成功',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jdlx_recharge
-- ----------------------------
INSERT INTO `jdlx_recharge` VALUES ('1', '0.01', '1');
INSERT INTO `jdlx_recharge` VALUES ('2', '0.01', '1');
INSERT INTO `jdlx_recharge` VALUES ('3', '0.01', '1');
INSERT INTO `jdlx_recharge` VALUES ('4', '0.01', '1');
INSERT INTO `jdlx_recharge` VALUES ('5', '0.01', '1');
INSERT INTO `jdlx_recharge` VALUES ('6', '0.01', '1');
INSERT INTO `jdlx_recharge` VALUES ('7', '0.01', '1');
INSERT INTO `jdlx_recharge` VALUES ('8', '0.01', '1');
INSERT INTO `jdlx_recharge` VALUES ('9', null, '1');
INSERT INTO `jdlx_recharge` VALUES ('10', null, '1');
INSERT INTO `jdlx_recharge` VALUES ('11', null, '1');
INSERT INTO `jdlx_recharge` VALUES ('12', '0.01', '1');

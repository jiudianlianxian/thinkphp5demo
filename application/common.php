<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
error_reporting(E_ERROR | E_PARSE);

use think\Loader;

/**
 * @return bool判断是不是手机访问 返回true代码是
 */
function isMobile()
{
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
        return true;

    //此条摘自TPM智能切换模板引擎，适合TPM开发
    if (isset ($_SERVER['HTTP_CLIENT']) && 'PhoneClient' == $_SERVER['HTTP_CLIENT'])
        return true;
    //如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
        //找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
    //判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT'])) {
        $clientkeywords = array(
            'nokia', 'sony', 'ericsson', 'mot', 'samsung', 'htc', 'sgh', 'lg', 'sharp', 'sie-', 'philips', 'panasonic', 'alcatel', 'lenovo', 'iphone', 'ipod', 'blackberry', 'meizu', 'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm', 'operamini', 'operamobi', 'openwave', 'nexusone', 'cldc', 'midp', 'wap', 'mobile'
        );
        //从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return true;
        }
    }
    //协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT'])) {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
            return true;
        }
    }
    return false;
}

function get_client_ip()
{
    $cip = 'unknown';
    if ($_SERVER["REMOTE_ADDR"]) {
        $cip = $_SERVER["REMOTE_ADDR"];
    } elseif (getenv("REMOTE_ADDR")) {
        $cip = getenv("REMOTE_ADDR");
    }
    return $cip;

}

/**
 * 会员充值时，重新调整到个网站的会员个人中心
 * @return string
 */
function getPersonUrl(){
    $urlList = parse_url($_SERVER['HTTP_REFERER']);
    $controList= explode("/",$urlList['path']);
    $contro = $controList[1];
    $lastUrl = $urlList['scheme']."://".$urlList['host']."/".$contro;
    return $lastUrl;
}


/**
 * 发送短信
 * @param $phone
 * @param $data 替换的内容
 * @param $id 模板ID
 * @return string
 */
function sendYunTongXunCode($phone,$data,$id){
    Loader::import('SendMsg.SendTemplateSMS');
    $r = sendTemplateSMS( $phone, $data, $id);
    if ($r == 'success') {
        return '1';
    }else {
        return '2';
    }
}

/**
 * 发送短信提醒
 * @return [type] [description]
 */
function sendMsg($phone, $code, $templateParam = null)
{
    $request = new \Dysms\AliyunSms();
    $signName = "灰姑娘幼教";//短信签名
    $templateCode = $code;//短信模板Code
    $phoneNumbers = $phone;//短信接收号码
    $info = $request->sendSms($signName, $templateCode, $phoneNumbers, $templateParam);
    return $info;
}
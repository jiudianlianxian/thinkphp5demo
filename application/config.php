<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    // +----------------------------------------------------------------------
    // | 应用设置
    // +----------------------------------------------------------------------

    // 应用命名空间
    'app_namespace'          => 'app',
    // 应用调试模式
    'app_debug'              => true,
    // 应用Trace
    'app_trace'              => false,
    // 应用模式状态
    'app_status'             => '',
    // 是否支持多模块
    'app_multi_module'       => true,
    // 入口自动绑定模块
    'auto_bind_module'       => false,
    // 注册的根命名空间
    'root_namespace'         => [],
    // 扩展函数文件
    'extra_file_list'        => [THINK_PATH . 'helper' . EXT],
    // 默认输出类型
    'default_return_type'    => 'html',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return'    => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler'  => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler'      => 'callback',
    // 默认时区
    'default_timezone'       => 'PRC',
    // 是否开启多语言
    'lang_switch_on'         => false,
    // 默认全局过滤方法 用逗号分隔多个
    'default_filter'         => '',
    // 默认语言
    'default_lang'           => 'zh-cn',
    // 应用类库后缀
    'class_suffix'           => false,
    // 控制器类后缀
    'controller_suffix'      => false,

    'exception_ignore_type'  => E_NOTICE|E_USER_NOTICE,

    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------

    // 默认模块名
    'default_module'         => 'admin',
    // 禁止访问模块
    'deny_module_list'       => ['common'],
    // 默认控制器名
    'default_controller'     => 'Index',
    // 默认操作名
    'default_action'         => 'index',
    // 默认验证器
    'default_validate'       => '',
    // 默认的空控制器名
    'empty_controller'       => 'Error',
    // 操作方法后缀
    'action_suffix'          => '',
    // 自动搜索控制器
    'controller_auto_search' => false,

    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------

    // PATHINFO变量名 用于兼容模式
    'var_pathinfo'           => 's',
    // 兼容PATH_INFO获取
    'pathinfo_fetch'         => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],
    // pathinfo分隔符
    'pathinfo_depr'          => '/',
    // URL伪静态后缀
    'url_html_suffix'        => 'html',
    // URL普通方式参数 用于自动生成
    'url_common_param'       => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type'         => 0,
    // 是否开启路由
    'url_route_on'           => true,
    // 路由使用完整匹配
    'route_complete_match'   => false,
    // 路由配置文件（支持配置多个）
    'route_config_file'      => ['route'],
    // 是否强制使用路由
    'url_route_must'         => false,
    // 域名部署
    'url_domain_deploy'      => false,
    // 域名根，如thinkphp.cn
    'url_domain_root'        => '',
    // 是否自动转换URL中的控制器和操作名
    'url_convert'            => true,
    // 默认的访问控制器层
    'url_controller_layer'   => 'controller',
    // 表单请求类型伪装变量
    'var_method'             => '_method',
    // 表单ajax伪装变量
    'var_ajax'               => '_ajax',
    // 表单pjax伪装变量
    'var_pjax'               => '_pjax',
    // 是否开启请求缓存 true自动缓存 支持设置请求缓存规则
    'request_cache'          => false,
    // 请求缓存有效期
    'request_cache_expire'   => null,
    // 全局请求缓存排除规则
    'request_cache_except'   => [],

    // +----------------------------------------------------------------------
    // | 模板设置
    // +----------------------------------------------------------------------

    'template'               => [
        // 模板引擎类型 支持 php think 支持扩展
        'type'         => 'Think',
        // 模板路径
        'view_path'    => '',
        // 模板后缀
        'view_suffix'  => 'html',
        // 模板文件名分隔符
        'view_depr'    => DS,
        // 模板引擎普通标签开始标记
        'tpl_begin'    => '{',
        // 模板引擎普通标签结束标记
        'tpl_end'      => '}',
        // 标签库标签开始标记
        'taglib_begin' => '<',
        // 标签库标签结束标记
        'taglib_end'   => '>',
    ],

    // 视图输出字符串内容替换
    'view_replace_str'       => [
        '__STATIC__' =>  '/static'
    ],
    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => THINK_PATH . 'tpl' . DS . 'dispatch_jump.tpl',
    'dispatch_error_tmpl'    => THINK_PATH . 'tpl' . DS . 'dispatch_jump.tpl',

    // +----------------------------------------------------------------------
    // | 异常及错误设置
    // +----------------------------------------------------------------------

    // 异常页面的模板文件
    'exception_tmpl'         => THINK_PATH . 'tpl' . DS . 'think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'          => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'         => false,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle'       => '',

    // +----------------------------------------------------------------------
    // | 日志设置
    // +----------------------------------------------------------------------

    'log'                    => [
        // 日志记录方式，内置 file socket 支持扩展
        'type'  => 'File',
        // 日志保存目录
        'path'  => LOG_PATH,
        // 日志记录级别
        'level' => [],
    ],

    // +----------------------------------------------------------------------
    // | Trace设置 开启 app_trace 后 有效
    // +----------------------------------------------------------------------
    'trace'                  => [
        // 内置Html Console 支持扩展
        'type' => 'Html',
    ],

    // +----------------------------------------------------------------------
    // | 缓存设置
    // +----------------------------------------------------------------------

    'cache'                  => [
        // 驱动方式
        'type'   => 'File',
        // 缓存保存目录
        'path'   => CACHE_PATH,
        // 缓存前缀
        'prefix' => '',
        // 缓存有效期 0表示永久缓存
        'expire' => 0,
    ],

    // +----------------------------------------------------------------------
    // | 会话设置
    // +----------------------------------------------------------------------

    'session'                => [
        'id'             => '',
        // SESSION_ID的提交变量,解决flash上传跨域
        'var_session_id' => '',
        // SESSION 前缀
        'prefix'         => 'think',
        // 驱动方式 支持redis memcache memcached
        'type'           => '',
        // 是否自动开启 SESSION
        'auto_start'     => true,
    ],

    // +----------------------------------------------------------------------
    // | Cookie设置
    // +----------------------------------------------------------------------
    'cookie'                 => [
        // cookie 名称前缀
        'prefix'    => '',
        // cookie 保存时间
        'expire'    => 0,
        // cookie 保存路径
        'path'      => '/',
        // cookie 有效域名
        'domain'    => '',
        //  cookie 启用安全传输
        'secure'    => false,
        // httponly设置
        'httponly'  => '',
        // 是否使用 setcookie
        'setcookie' => true,
    ],

    //分页配置
    'paginate'               => [
        'type'      => 'bootstrap',
        'var_page'  => 'page',
        'list_rows' => 15,
    ],
    'alipay'=>[
        //应用ID,您的APPID。
        "app_id" => '2018040202492981',
        //商户私钥
        "merchant_private_key" => 'MIIEowIBAAKCAQEA0OZRIkj72FKAwCRDJUlhAPOz6aKus6vPQ/JqmzGKYVkQnAnyGZIQ7ly6bUoe7aqejul+ch7YJ5xYVqOqlyw7g/N8PFqccgkF13pIxGSlIrabUJad4mZ+2VZ3rJU9FAnPWU4dFi4s96kVem2ai1ezwXlrBgL5NNEWfrQ3fTpDqvZ2vgq4b/HZ07ecH8EbnCg9q4FsVyKx/in7m+8PYJqg/nRSlTB+fwBA0FpuyQuF2uqW8X1EQb2Moo9W0lCche0dDxpSxvbT6zW4IVFp/SLzwBoBF0vXBoAiFG+CBqa5v+FBlmLpbHov2jSzdsEVrZ/nKRNQ46H+LI+10hB/3OTVVwIDAQABAoIBAAFiJRNnDFPoZ/Xdx5wNmljZRE29yA5kiuDWUCJq95Ghd1yxLCtDLl2Fu65RmVbVlGs53J6A2cmZXXRlbRHHNzWufRohN3wB10oQAz9Zs0ZBk1G7TZxcNWbXDb/U1Sy2y+q5e7PT5nRVaFEBPM7Da+tSFIVbq67P26hHJT7tDdmp+RkJRnl6oVfDMajjufcjMOMxKlsISC/6472uiUQW7cvk5bhQToXki5Nx/kbkisM82AeHFbW/8PWk1hrGufTvIRhsXYsbBi66CdvAcESrr3mnFuc9LIF6B0P7NUI0CdONwTqbhUTh1pXySFCSycsJVDuFMxAMV7PxaH6pKULu0wECgYEA+T5JloMyRmpndYangFktbchhV54ZnKgMf0jGzoC3jHGRwBWeVYKD2h8jM1O3O4A5f4st1yZOgZR1NeeKoU60DaG0vnWxZQ9R5KBJSUz1qvFJdqkhtmrCU4mghab2xm+ahrKxBtEbTnnwTNQSzDFLT58e4svVfCo/5q5KxZhdf5cCgYEA1pAM7GRtsKw9nbNqgMxTRtA38Tbjytv35im0FNsNnheu5F1X3C/Df3jX1qjBBz1uR4yVesWFCh69FCsrAVCJQ5ldV0itqyv7E1U08gZS8xQV0QUfqRKUFCPkq1Hx/1n2DBOfFRjyqOBYTVs/LUaUum35/BqCVQLWChXRkhoCEEECgYA3LN7Ffw2mIO/+SGzUMOmpn+5MoycwUtNRah74n+4CHTISv7ZpX5cotjPorOOnPF9+KKVhN42xidfYswUxaGbbjayhbRR0Nsv+PK18mJjZ2shUiGR6KXATZigu7iM7EoG2x+AAjlHpMX1MxLXUliUB30vLESyVbBTxUCwlSfGIyQKBgEwbAFqBLN8z2Q8s6qQQ1aESjiVDQP1dlMrRFA0pveTLpb9e1KFHV+LJLFX5ua4lvg4m8YaAtTXnvU2AGlr3BBk0NwWUw/UpPn+XA5atY5LRFwPy+03vRxL1wFB8Wogmkr2p4A2NsdTw7qFRslEO7gZKqBciGASrYtxk7NcT3pOBAoGBAI+glpnmgp3EE9POeZ6Lj8FAZ8JqFhFNs6FZ6KfXpI4OKAOTGtJmYBhpOZDoqba+yC/96HXHXqNzn5nCaocD2kKvRwmlEuH2dXWXrdICiMcMbfgsxGb/GMTndpangOv7LVNOfaM8Uvwwl/Mv02lk49G24Gz6KtE+6DjT2J85f0PJ',
        //编码格式
        "charset" => "UTF-8",
        //签名方式
        "sign_type" => 'RSA2',
        //支付宝网关
        "gatewayUrl" => "https://openapi.alipay.com/gateway.do",
        //支付宝公钥
        "alipay_public_key" => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhdIqurBWbg8m6w7gdMycHbnl+5NB/2688bsn04Uu8+TqfVzq4++Y4zbyYzmczEdqzEV7FC95m/bp+j+RYfRTawHYZsIZ0sAgSA9/IZHIypU3oX/pKYmkmAqIfjyXBps0l89L3AlRYuWgDTnytiFmFh7ELdba1UYkzG/Lz7cqsyfQiLy071PodL46mpgS6fxnfyMAg+XHe92Tbm3A0v14qvVG+2cAlC0whYNcIfYkRS1kgKwH+RMbw8w66VwtkJ4KRJxakBWZ1UD0+riTIL7hM9OO2/+RWj4pj61xt5qbQbApKwBBlFWT5e8x+9psVPL8LRNBKOgu7bvWGJrCbmktSwIDAQAB'
    ]
//    'alipay'=>[
//        //应用ID,您的APPID。
//        "app_id" => '2016091200496911',
//        //商户私钥
//        "merchant_private_key" => 'MIIEpAIBAAKCAQEAnnlG7jEdsx4yhu55L9893sgURC6egS/EyOqm9VOeO5TC2ObJotPP4RbgfKcdoGs2FdJGgaKwN88qd8F0ZrnFBrx3gUuTukd8TGwQpOskgyA/eGplM7CqAX1ZbaC2/svJ948ifGb/GisHMy0lb+GLEaczeJpGntReK2FYYWxnKPUfduqPSLCNif4oPwK4eqXhivTLV9E0Vo8gyiHlri6cyANTJpWEwqyFSCt6KsxVrbVIlZYTTg4kLuIleQaILEqWxkHU8ZR5KbXHdFX98DzdRtiHQwKoLb5coR4dd2gmS8JFXcyBJRdAWpYP7vYU0xFvvXua9vn8VcPinSvVqhrgcQIDAQABAoIBADTuiTSSDzt+wJ21FaFZzY6WAb7sW2EHFZGOXwneTiHUhGhyXbMXUH38aCr6nriwVrZfL9GpB3b8qVCquzh8oESpurjjB4rWybu09LJBIEb0BH8x+m+TeJVayv4FClp8sw8PLhOjS13wNU1wrOp7yTmyQUztKt1uTa3rq9uOm75f6vM0SWt9q0DnpaetzzH7oUcoAtSIBFiHnxFsMItiGQ/NDwdtfbaHXXYF5I6thvo23biFBmxhdXlVqAc1l66uGKkX2qkR/tl8WwHdntDhnc8A/TcO3HZmWw+JikJjhUcpCUmkA+0UQ0cWgCMq1x8fh3H0Jz1ukP0l1w8qKKIpFUkCgYEAzrvUSFfxFTVLSQqTfAFU/xZXeJC0fv7YyOMOTPkN3ikFXDNFm4w7PjCswxQIW/siRqHbxoKm9CBdy1Vhf5mOgQjcj5U75KRaUM1Czmhi73zUNncXpTt8rRast/lSPL7JpQ8n5S9WV7INVw8LYXvmr0XruCQV5fL55/I0nz6c9vcCgYEAxD1CbfT9+2BiDdQNSx5GEFY9Ww0ngQ9wRcltV9RedvrLjNlj61FH/rs09fQl14E/S/bUcEicANjZDbcNA3iWb2zh3uE3EE4ewvJ51cFWin4hpMnP4pVp1CWaYKbVPmaD0BuXbCl9Qv8yBpQ9YTJGHHD4inZjFqWCkW0J+TSTgdcCgYEAhhId+8S8nqpvZrErMLKUctIEbDY8OFgCM1W9QoIKUld8GG1P6S6qhQyAHgtx2GUnt3Lk2vqEV7Jd4a4/iscFOf2LPLrlmsRTpi9X8hCUiQgFgET/2rKXipE39iAFC0wX7anEj8eBUWIxcJJbKR93XZgU5NjSMsy+8YOMSPIMTcUCgYEArfbolvWi2l/dTQj5J+iUkBMDnV4R8TWwgA1K45ueirdWD20ASTHrMwTetFNV4K/xXvRX65pSweV6tc/at4MFSY4qxhy9ZbT7TDMMktQsVeXvCVjm86UlBi63por/a6wRmkF7gSVeqC/3NyHfL/UTdk8n5geBXCgpkaYqO0PCqE0CgYB+UyhQnf79muGRvOXmfnaoTbSYkE1BeN54tvUcGuA/FReIF3tLSOa7PuHkFpgeANr/X2akV4e50ho3609tRkSbs67DHbhib++GQerS+sfTIDFR+fGbRJn8etIJNL1HGVu1wSoyxYfHPFUVEVzU4TJP2RSFMfKYnR+0NEm/krZiew==',
//        //编码格式
//        "charset" => "UTF-8",
//        //签名方式
//        "sign_type" => 'RSA2',
//        //支付宝网关
//        "gatewayUrl" => "https://openapi.alipaydev.com/gateway.do",
//        //支付宝公钥
//        "alipay_public_key" => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0QdzA+6D5wutq58WwTBDVFDz7DMl7KzaCQkPYnDmHYaibFjA+C5Y3vaJTzAAHiPi21KzZxjvrjyyf7HaPqKajQrybCWXs28XBoxqInX851fIYA0EEYjnofSh+s3pqL7SgcBcmT+DRbtRJ7uLXFiBGcg07tdcvSfqCFCg3uQhC66VXiAuKlFi0+2cYnvBMuKxvPFKQeai4upxPlCDBWiTBfB1MMqDDZNs/mNG48YsIZ0KzJJEHX7EFHuR7xFhi/OoDuMryfpIDF4DntfNT0XlGA0JIpfXNS61RZVw0j1KZiRIM4cwQSNaqmDAjZGQjZBvVH+KDhXBLGx+nsrRoa/t1wIDAQAB',
//        //支付路径
//        "zhifu_url"=>'http://guan.hgnhoutai.com'
//    ]
];

<?php
/**
 * Created by PhpStorm.
 * function: 处理第三方登录：微信登录
 * User: yule
 * Date: 2018/3/29 0029
 * Time: 下午 6:48
 */

namespace app\admin\controller;
use think\Controller;
use think\Loader;
use Wxlogin\Wxlogin;
ini_set('date.timezone','Asia/Shanghai');
header("Access-Control-Allow-Origin: *");
class Wechat extends Controller
{
    public function wxlogin(){
        $config = config('wx');

        $newclass=new Wxlogin();
        //var_dump($newclass);
        $result=$newclass->get_login_code($config['appid']);
        echo $result;
    }
}
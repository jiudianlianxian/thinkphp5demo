<?php
/**
 * Created by PhpStorm.
 * function: 处理第三方登录：微信登录
 * User: yule
 * Date: 2018/3/29 0029
 * Time: 下午 6:48
 */
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use Wxlogin\Wxlogin;
use think\session;
ini_set('date.timezone','Asia/Shanghai');
header("Access-Control-Allow-Origin: *");
class Callback extends Controller
{
    public function index(){
        //验证CSRF攻击
        if($_GET['state']!=session('wx_state')){
            exit("5001");
        }
        $AppID = 'wx35ea3dd45f005dc8';
        $AppSecret = 'e0056735f375c2075a678e2125f8e02e';
        $url='https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$AppID.'&secret='.$AppSecret.'&code='.$_GET['code'].'&grant_type=authorization_code';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $json =  curl_exec($ch);
        curl_close($ch);
        $arr=json_decode($json,1);
        //得到 access_token 与 openid
        print_r($arr);
        $url='https://api.weixin.qq.com/sns/userinfo?access_token='.$arr['access_token'].'&openid='.$arr['openid'].'&lang=zh_CN';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $json =  curl_exec($ch);
        curl_close($ch);
        $arr=json_decode($json,1);
       // 得到 用户资料
       print_r($arr);
    }
}
<?php
/**
 * 公共控制器.
 * User: Sun
 * Date: 2017/12/25
 * Time: 11:08
 */
namespace app\admin\controller;
use think\Controller;
use think\Loader;
ini_set('date.timezone','Asia/Shanghai');
header("Access-Control-Allow-Origin: *");
class Sendmsg extends Controller
{
    function index(){
        return $this->fetch();
    }

    /**
     * 云通讯短信平台
     * @return \think\response\Json
     */
    function yunTongXun(){
        $phone = input("phone");
        $yanzheng = rand(100000, 999999);
        $flag = sendYunTongXunCode($phone,array($yanzheng),'201785');
        if($flag=='1'){
            session('yanzhengma' . $phone,$yanzheng);
            return json(["status"=>"1","message"=>"发送成功，请查看手机！"]);
        }else{
            return json(["status"=>"2","message"=>"发送失败，请联系客服！"]);
        }
    }
    /**
     * 阿里云短信平台
     * @return \think\response\Json
     */
    function dySms(){
        # 手机号
        $phone = input("phone");
        # 模板code
        $code = "SMS_120520251";
        # 验证码
        $yanzheng = (string)rand(100000,999999);
        $templateParam = array("code"=> $yanzheng);
        $request = sendMsg($phone,$code,$templateParam);
        if($request->Code == 'OK'){
            session('yanzhengma' . $phone,$yanzheng);
            return json(["status"=>"1","message"=>"发送成功，请查看手机！"]);
        }else{
            return json(["status"=>"2","message"=>"发送失败，请联系客服！"]);
        }
    }

    /**
     * 获取手机验证码、验证
     */
    public function sendMsgBack(){
        $phone = input("phone");
        $code = input("code");
        if($code == session('yanzhengma' . $phone)){
            return json(["status"=>"1","message"=>"验证码正确！"]);
        }else{
            return json(["status"=>"2","message"=>"验证码错误！"]);
        }
    }
}
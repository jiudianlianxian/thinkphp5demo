<?php
/**
 * 微信支付功能，包含微信PC端支付，手机浏览器H5支付
 * 使用需修改extend/Wxpay/lib/WxPayconfig.php中的APPID、MCHID、KEY参数
 * 以及替换.extend/Wxpay/cert中的证书文件
 * User: Sun
 * Date: 2017/12/25
 * Time: 11:08
 */
namespace app\admin\controller;
use think\Controller;
use think\Db;
ini_set('date.timezone','Asia/Shanghai');
header("Access-Control-Allow-Origin: *");
class Wxpay extends Controller
{
    /**
     * 微信支付
     * @return \think\response\Json
     */
    function recharge(){
        # 充值金额
        $money = input("money");
        $data["money"] = $money;
        $data["status"] = '1';
        $rechargeId = Db::name("recharge")->insertGetId($data);
        $params = [
            'body' => '充值',
            'out_trade_no' => str_pad($rechargeId,32,'0',STR_PAD_LEFT), # 订单号为32位字符串 唯一
            'total_fee' => $data["money"]*100, # 订单总金额，单位为分，所有在*100
        ];
        if(isMobile()){
            // 手机浏览器H5支付，会打开新的页面。为了能在支付成功后回到发起支付的页面，所有这里要把发起支付的地址传递过去
            $result = \wxpay\WapPay::getPayUrl($params,'http://'.$_SERVER['HTTP_HOST']."/Admin/Wxpay/rechargeNotify/?rechargeId=$rechargeId&lastUrl=".base64_encode($_SERVER["HTTP_REFERER"]));
            echo "<script language='javascript'>window.location.href='$result';</script>";
            exit();
        }else{
            // 扫码支付
            $params["product_id"] = $rechargeId; # trade_type=NATIVE时（即扫码支付），此参数必传。此参数为二维码中包含的商品ID
            $params["notify_url"] = 'http://'.$_SERVER['HTTP_HOST']."/Admin/Wxpay/rechargeNotify"; # 回调地址
            $result = \wxpay\NativePay::getPayImage($params);
            $this->assign("result",$result);
            $this->assign("id",$rechargeId);
            return $this->fetch();
        }
    }

    /**
     * 微信支付的异步处理
     * @throws \WxPayException
     */
    public function rechargeNotify()
    {
        if(isMobile()) {
            $rechargeId = input("rechargeId");
            $lastUrl = base64_decode(input("lastUrl"));
            // 查询订单是否成功
            $result = \wxpay\Query::exec(str_pad($rechargeId,32,'0',STR_PAD_LEFT));
            if($result['return_code'] =='SUCCESS' && $result['return_msg'] == 'OK'&&$result['trade_state'] == 'SUCCESS'){
                // 若订单成功，查询订单信息
                $order = Db::name('recharge')->where('id', $rechargeId)->find();
                if($order){
                    // 更新订单状态为成功
                    Db::name('recharge')->where('id', $order['id'])->update(['status'=>2]);
                    $this->redirect($lastUrl);
                    exit();
                }else{
                    // 返回发起页面
                    $this->redirect($lastUrl);
                    exit();
                }
            }else{
                // 因为H5支付，不会自动查询订单状态，需要通过订单接口自行进行查询，这样的话可能因为网络延迟等原因，造成一次查询未返回正确结果，所以需要多次进行查询
                $nums = input("nums")?input("nums"):1;
                $nums += 1;
                if($nums == 5){
                    $this->redirect($lastUrl);
                    exit();
                }else{
                    sleep(2);
                    $dataUrl = 'http://'.$_SERVER['HTTP_HOST']."/Wxpay/buyProductNotify/?recharge=$rechargeId&nums=$nums&lastUrl=".base64_encode($lastUrl);
                    echo "<script language='javascript'>window.location.href='$dataUrl';</script>";
                    exit();
                }

            }
        }else{
            $notify = new \wxpay\RechargeNotify();
            $notify->Handle();
        }
    }
    function queryBuyState(){
        $id = input("id");
        $info = Db::name('recharge')->where('id', $id)->find();
        return json(["status"=>$info["status"]]);
    }
}
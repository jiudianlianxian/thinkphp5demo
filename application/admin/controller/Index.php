<?php
/**
 * 公共控制器.
 * User: Sun
 * Date: 2017/12/25
 * Time: 11:08
 */
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Request;
ini_set('date.timezone','Asia/Shanghai');
header("Access-Control-Allow-Origin: *");
class Index extends Controller
{
    function index(){
        return $this->fetch();
    }
}
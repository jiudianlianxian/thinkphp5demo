<?php
/**
 * 阿里云OSS上传文件
 */
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Loader;
use OSS\OssClient;
ini_set('date.timezone','Asia/Shanghai');
header("Access-Control-Allow-Origin: *");
header("Content-type: text/html; charset=utf-8");
class Alioss extends Controller
{
    /**
     * 上传文件
     */
    function uploadFile(){
        Loader::import('aliyunoss.OssCommon');
        $bucketName = \OssCommon::getBucketName();
        $ossClient = \OssCommon::getOssClient();
        # 上传到服务器上的文件名
        $objectName = '2018-3-26/simple.mp4';
        # 创建目录
        $ossClient->createObjectDir($bucketName,'2018-3-26');
        # 上传文件
        $ossClient->uploadFile($bucketName, $objectName, "D:\\1.mp4");
        # 判断文件是否存在
        $doesExist = $ossClient->doesObjectExist($bucketName, $objectName);
        # 获取文件地址
        $url = $ossClient->signUrl($bucketName, $objectName,3600);
        var_dump($url);
    }

    /**
     * 下载文件到本地
     */
    function getFile(){
        Loader::import('aliyunoss.OssCommon');
        $bucketName = \OssCommon::getBucketName();
        $ossClient = \OssCommon::getOssClient();
        # 判断文件是否存在
        $doesExist = $ossClient->doesObjectExist($bucketName, "2018-3-26/simple.jpg");
        if($doesExist){
            # 如果存在，进行下载
            $options = array(
                OssClient::OSS_FILE_DOWNLOAD => 'D:\\simple.jpg', # 下载到本地的路径
            );
            $ossClient->getObject($bucketName, "2018-3-26/simple.jpg",$options);
            if(file_exists('D:\\simple.jpg')){
                echo "下载成功";
            }
        }
    }
    /**
     * 下载文件到内存
     */
    function getFileContent(){
        Loader::import('aliyunoss.OssCommon');
        $bucketName = \OssCommon::getBucketName();
        $ossClient = \OssCommon::getOssClient();
        $objectName = "2018-3-26/simple.jpg";
        # 判断文件是否存在
        $doesExist = $ossClient->doesObjectExist($bucketName,$objectName );
        if($doesExist){
            $content = $ossClient->getObjectMeta($bucketName, $objectName);
            $filename = pathinfo($content["oss-request-url"])['basename'];
            $url = $ossClient->signUrl($bucketName, $objectName,3600);
            $fileinfo = get_headers($url,true);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$filename );
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . $fileinfo["Content-Length"]);
            ob_clean();
            flush();
            readfile($url);
        }
    }
    /**
     * 删除单个文件
     */
    function delFile(){
        Loader::import('aliyunoss.OssCommon');
        $bucketName = \OssCommon::getBucketName();
        $ossClient = \OssCommon::getOssClient();
        # 删除文件
        $ossClient->deleteObject($bucketName, "2018-3-26/simple.jpg");
    }

    /**
     * 删除多个文件
     */
    function delAllFile(){
        Loader::import('aliyunoss.OssCommon');
        $bucketName = \OssCommon::getBucketName();
        $ossClient = \OssCommon::getOssClient();
        $fileList = [ "2018-3-26/simple.jpg", "2018-3-26/simple1.jpg"];
        # 删除文件
        $ossClient->deleteObjects($bucketName,$fileList);
    }

    /**
     * 复制文件
     */
    function copyFile(){
        Loader::import('aliyunoss.OssCommon');
        $bucketName = \OssCommon::getBucketName();
        $ossClient = \OssCommon::getOssClient();
        $ossClient->copyObject($bucketName,"2018-3-26/simple.jpg",$bucketName,"2018-3-26/simple1.jpg");
        $doesExist = $ossClient->doesObjectExist($bucketName, "2018-3-26/simple1.jpg");
        if($doesExist) {
            echo "拷贝成功";
        }
    }

    /**
     * 设置文件访问权限
     * 默认	Objec是遵循Bucket的读写权限，即Bucket是什么权限，Object就是什么权限，Object的默认权限	default
     * 私有读写	Object是私有资源，即只有该Object的Owner拥有该Object的读写权限，其他的用户没有权限操作该Object	private
     * 公共读私有写	Object是公共读资源，即非Object Owner只有Object的读权限，而Object Owner拥有该Object的读写权限	public-read
     * 公共读写	Object是公共读写资源，即所有用户拥有对该Object的读写权限	public-read-write
     */
    function putFileAcl(){
        Loader::import('aliyunoss.OssCommon');
        $bucketName = \OssCommon::getBucketName();
        $ossClient = \OssCommon::getOssClient();
        $ossClient->putObjectAcl($bucketName, "2018-3-26/simple1.jpg",'private');
    }

    /**
     * 获取文件访问权限
     */
    function getFileAcl(){
        Loader::import('aliyunoss.OssCommon');
        $bucketName = \OssCommon::getBucketName();
        $ossClient = \OssCommon::getOssClient();
        $result = $ossClient->getObjectAcl($bucketName, "2018-3-26/simple1.jpg");
        var_dump($result);
    }

}
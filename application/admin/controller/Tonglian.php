<?php
/**
 * 支付宝PC端扫码支付、手机浏览器支付
 * 使用时需修改application/config.php中的alipay数组中的参数
 * User: Sun
 * Date: 2017/12/25
 * Time: 11:08
 */
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Loader;
ini_set('date.timezone','Asia/Shanghai');
header("Access-Control-Allow-Origin: *");
class Tonglian extends Controller
{
    /**
     * 通联支付
     */
    function index(){
        // 提交地址 测试环境：http://ceshi.allinpay.com/gateway/index.do 生产环境：https://service.allinpay.com/gateway/index.do
        $serverUrl = 'http://ceshi.allinpay.com/gateway/index.do';
        $param = [];
        #字符集 1 代表 UTF-8、2 代表 GBK、3 代表 GB2312
        $param["inputCharset"]= '1';
        #客户的取货地址
        $param["pickupUrl"]=  'http://'.$_SERVER['HTTP_HOST']."/Admin/Tonglian/pickUrl";
        # 通知商户网站支付结果的 url 地址
        $param["receiveUrl"]= 'http://'.$_SERVER['HTTP_HOST']."/Admin/Tonglian/receiveUrl";
        # 网关接收支付 请求接口版本 固定填v1.0
        $param["version"]= 'v1.0';
        # 签名类型 默认填 1，固定选择值：0、1； 0 表示订单上送和交易结果通知都使用 MD5 进行签名 1 表示商户用使用 MD5 算法验签上送订单，通联交 易结果通知使用证书签名
        $param["signType"]= 1;
        # 数字串，商户在通联申请开户的商户号
        $param["merchantId"]= '100020091218001';
        # 商户订单号
        $param["orderNo"]= time().rand(0,99999);
        # 整型数字，金额与币种有关  如果是人民币，则单位是分，即 10 元提交时金额 应为 1000 如果是美元，单位是美分，即 10 美元提交时金额 为 1000
        $param["orderAmount"]= input("money")*100;
        # 币种 默认填 0 0 和 156 代表人民币、840 代表美元、344 代表港币， 跨境支付商户不建议使用 0
        $param["orderCurrency"]= '0';
        # 商户订单提交 时间  日期格式：yyyyMMDDhhmmss，例如： 20121116020101
        $param["orderDatetime"]= date('YmdHis',time());
        # 支付方式 固定选择值：0、1、4、11、23、28 接入互联网关时，默认为间连模式，填 0 若需接入外卡支付，只支持直连模式，即固定上送 payType=23，issuerId=visa 或 mastercard 0 代表未指定支付方式，即显示该商户开通的所有 支付方式 1 个人储蓄卡网银支付 4 企业网银支付 11 个人信用卡网银支付 23 外卡支付
        $param["payType"]= '0';
        # 用于计算signMsg的key值
        $key='1234567890';
        // 生成签名字符串。
        $bufSignSrc="";
        if($param["inputCharset"] != "")
            $bufSignSrc=$bufSignSrc."inputCharset=".$param["inputCharset"]."&";
        if($param["pickupUrl"] != "")
            $bufSignSrc=$bufSignSrc."pickupUrl=".$param["pickupUrl"]."&";
        if($param["receiveUrl"] != "")
            $bufSignSrc=$bufSignSrc."receiveUrl=".$param["receiveUrl"]."&";
        if($param["version"] != "")
            $bufSignSrc=$bufSignSrc."version=".$param["version"]."&";
        if($param["signType"] != "")
            $bufSignSrc=$bufSignSrc."signType=".$param["signType"]."&";
        if($param["merchantId"] != "")
            $bufSignSrc=$bufSignSrc."merchantId=".$param["merchantId"]."&";
        if($param["orderNo"] != "")
            $bufSignSrc=$bufSignSrc."orderNo=".$param["orderNo"]."&";
        if($param["orderAmount"] != "")
            $bufSignSrc=$bufSignSrc."orderAmount=".$param["orderAmount"]."&";
        if($param["orderCurrency"] != "")
            $bufSignSrc=$bufSignSrc."orderCurrency=".$param["orderCurrency"]."&";
        if($param["orderDatetime"] != "")
            $bufSignSrc=$bufSignSrc."orderDatetime=".$param["orderDatetime"]."&";
        if($param["payType"] != "")
            $bufSignSrc=$bufSignSrc."payType=".$param["payType"]."&";
        $bufSignSrc=$bufSignSrc."key=".$key; //key为MD5密钥，密钥是在通联支付网关商户服务网站上设置。
        //签名，设为signMsg字段值。
        $signMsg = strtoupper(md5($bufSignSrc));
        $param["signMsg"] = $signMsg;
        $param["serverUrl"] = $serverUrl;
        $this->assign("data",$param);
        return $this->fetch();
    }

    /**
     * 客户的取货地址 同步通知
     */
    function pickUrl(){
        $return = input("post.");
        var_dump($return);
        if($return["payResult"] == '1'){
            echo "支付成功";
        }
    }

    /**
     * 通知商户网站支付结果的 url 地址  异步通知
     */
    function receiveUrl(){
        $return = input("param.");
        if($return["payResult"] == '1'){
            // 支付成功
            $f = fopen($_SERVER["DOCUMENT_ROOT"].'/receiveUrl.txt','w');
            fwrite($f,var_export(input("param."),true));
            fclose($f);
        }
    }
}
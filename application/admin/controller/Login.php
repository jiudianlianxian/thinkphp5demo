<?php
/**
 * Created by PhpStorm.
 * function: 处理第三方登录：微信登录
 * User: yule
 * Date: 2018/3/29 0029
 * Time: 下午 6:48
 */
namespace app\admin\controller;
use think\Controller;
use think\Loader;
use think\Session;
ini_set('date.timezone','Asia/Shanghai');
header("Access-Control-Allow-Origin: *");
class Login extends Controller
{
    public function login(){
        $AppID = 'wx35ea3dd45f005dc8';
        $AppSecret = 'e0056735f375c2075a678e2125f8e02e';
        session_start();
        //-------生成唯一随机串防CSRF攻击
        $state  = md5(uniqid(rand(), TRUE));
        session("wx_state",$state); //存到SESSION
        $redirect_url = urlencode('http://demo4.jiudianlianxian.com/admin/callback');
        $wxurl = 'https://open.weixin.qq.com/connect/qrconnect?appid='.$AppID.'&redirect_uri='.$redirect_url.'&response_type=code&scope=snsapi_login&state='.$state.'#wechat_redirect';
        header("Location: $wxurl");
    }
}